function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function currencyInWords(number)
{
	if(number == null) return "";
	var rupees = Math.trunc(number);
	var paise = (number % 1).toFixed(2).substring(2);
	var result = convertToWords(rupees) + " rupees ";
	if (paise != 0)
	result += convertToWords(paise) + " paise";
    return  result.split(" ");
}
function numberInWords(number)
{
	if(number == null) return "";
	var rupees = Math.trunc(number);
	var paise = (number % 1).toFixed(2).substring(2);
	var result = convertToWords(rupees);
	if (paise != 0)
	result += convertToWords(paise);
    return result.split(" ");	
}
function convertToWords(number)
{
    var Gn = Math.floor(number / 10000000);
    number -= Gn * 10000000; 
    var kn = Math.floor(number / 100000);
    number -= kn * 100000; 
    var Hn = Math.floor(number / 1000);
    number -= Hn * 1000; 
    var Dn = Math.floor(number / 100);
    number = number % 100;
    var tn= Math.floor(number / 10); 
    var one=Math.floor(number % 10); 
    var res = ""; 
    if (Gn>0)   { 
        res += (convertToWords(Gn) + " crore"); 
    } 
    if (kn>0) 
    { 
            res += (((res=="") ? "" : " ") + 
            convertToWords(kn) + " lakh"); 
    } 
    if (Hn>0) 
    { 
        res += (((res=="") ? "" : " ") +
            convertToWords(Hn) + " thousand"); 
    } 
    if (Dn) 
    { 
        res += (((res=="") ? "" : " ") + 
            convertToWords(Dn) + " hundred"); 
    } 
    var ones = Array("", "one", "two", "three", "four", "five", "six","seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen","fourteen", "fifteen", "sixteen", "seventeen", "eighteen","nineteen"); 
	var tens = Array("", "", "twenty", "thirty", "forty", "fifty", "sixty","seventy", "eighty", "ninety");     
    if (tn>0 || one>0) 
    { 
        if (!(res=="")) 
        { 
            res += " and "; 
        } 
        if (tn < 2) 
        { 
            res += ones[tn * 10 + one]; 
        } 
        else 
        { 

            res += tens[tn];
            if (one>0) 
            { 
                res += (" " + ones[one]); 
            } 
        } 
    }    
    if (res=="")
    { 
        res = "zero"; 
    } 
    return res;
}
function ConvertToIndian(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1].substring(0, 2) : '';
	var rgx = /(\d+)(\d{3})/;
	var z = 0;
	var len = String(x1).length;
	var num = parseInt((len/2)-1);

	while (rgx.test(x1))
	{
		if(z > 0)
		  x1 = x1.replace(rgx, '$1' + ',' + '$2');
		else
		{
		  x1 = x1.replace(rgx, '$1' + ',' + '$2');
		  rgx = /(\d+)(\d{2})/;
		}
		z++;
		num--;
		if(num == 0)
		{
		  break;
		}
	}
	return x1 + x2;
}

function capitalizeFirstLetter(string) {return string.charAt(0).toUpperCase() + string.slice(1);}

