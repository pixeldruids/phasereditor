﻿using System;
using System.Collections;
using System.Collections.Generic;
[Serializable]
public class BaseObject
{
    public string id { get; set; }
    public int x { get; set; }
    public int y { get; set; }
    public double timing { get; set; }
    public double delay { get; set; }

}
[Serializable]
public class SpriteAnimation : BaseObject
{
    public string sprite {get; set;}
    public bool loop {get; set;}
    public double? scale {get; set;}
    public double disappear{get; set;}
}
[Serializable]
public class TextAnimation : BaseObject
{
    public List<string> text {get; set;}
    public int sx {get; set;}
    public int sy {get; set;}
    public int size {get; set;}
    public string tween_type {get; set;}
    public bool add_audio_delay {get; set;}
    public bool ConvertToIndian {get; set;}
    public int? anchor {get; set;}

    public TextAnimation()
    {
        text = new List<string>();
    }
}
[Serializable]
public class Function
{
    public string fn {get; set;}
    public double delay {get; set;}
}
[Serializable]
public class SouldList
{
    public string id { get; set; }
    public string sound {get; set;}
    public SouldList(){}
}
[Serializable]
public class Tween : BaseObject
{
    public string obj {get; set;}
    public string tween_type {get; set;}
}
[Serializable]
public class Screen
{
    public List<SpriteAnimation> sprite_animations {get; set;}
    public List<TextAnimation> text_animations {get; set;}
    public List<SouldList> sound_list { get; set; }
    public List<Function> functions {get; set;}
    public List<Tween> tweens { get; set; }
    public string name {get; set;}
    public double timing {get; set;}
    public string BGColor{get; set;}
    public Screen()
    {
        name = "new";
        timing = -1;
        sprite_animations = new List<SpriteAnimation>();
        text_animations = new List<TextAnimation>();
        sound_list = new List<SouldList>();
        functions = new List<Function>();
        tweens = new List<Tween>();
    }
}
[Serializable]
public class Data
{
    public List<Screen> screens {get; set;}
    public Data()
    {
        screens = new List<Screen>();
    }
}