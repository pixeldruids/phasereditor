﻿using NAudio.Wave;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KfdGen
{
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public partial class Form1 : Form
    {
        Data mSource;
        GlobalSettings mSettings = new GlobalSettings();
        string _currentProject;
        public string currentProject { get { return _currentProject; } set {
                _currentProject = value;
                mSettings.LocalHostPath.Replace("\\", "/");
                Utility.CurrentProject = _currentProject;
                Utility.CurrentProjectPath = mSettings.LocalHostPath + "/" + _currentProject + "/";
                Utility.CurrentJsFolder = mSettings.LocalHostPath + "/" + _currentProject + "/js/";
                Utility.CurrentAssetsFile = Utility.CurrentJsFolder + "assets.js";
                AssetsData.Init(Utility.CurrentAssetsFile);
            } }
        int currentPage = 0;
        TreeNode currentNode = null;
        public static Assets AssetsData = new Assets();
        WaveOut waveOut = new WaveOut();

        public Form1()
        {
            InitializeComponent();
            mSettings.Init();
            this.treeView1.AfterSelect += TreeView1_Click;
            this.treeView1.LabelEdit = true;
            this.treeView1.AfterLabelEdit += TreeView1_AfterLabelEdit;
            this.webBrowser1.Navigated += WebBrowser1_Navigated;
            this.webBrowser1.ObjectForScripting = this;
            this.webBrowser1.AllowWebBrowserDrop = true ;
            this.propertyGrid1.Leave += PropertyGrid1_Leave;
            BrowserContextMenuStrip.ItemClicked += BrowserContextMenuStrip_ItemClicked;
        }

        private void TreeView1_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            for (int i = 0; i < mSource.screens.Count; i++)
            {
                if(e.Node.Tag.ToString() == mSource.screens[i].name)
                {
                    if (e.Node.Tag == null) return;
                    mSource.screens[i].name = e.Label;
                    e.Node.Tag = e.Label;
                }
            }
        }

        private void PropertyGrid1_Leave(object sender, EventArgs e)
        {
            SaveAndLoad(false);
        }

        private void WebBrowser1_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            LoadProject();
        }

        private void BrowserContextMenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Name == "ctxAddText")
            {
                using (AddTextDialog dialog = new AddTextDialog())
                {
                    if(dialog.ShowDialog() == DialogResult.OK) AddTextToCurrentPage(dialog.textAnim);
                }

            }
            if (e.ClickedItem.Name == "ctxAddSprite")
            {
                using (AddSprite dialog = new AddSprite(BrowserContextMenuStrip.Left - webBrowser1.Left, BrowserContextMenuStrip.Top - webBrowser1.Top))
                {
                    if (dialog.ShowDialog() == DialogResult.OK)
                        AddSpriteToCurrentPage(dialog.result);
                }

            }
        }

        void AddTextToCurrentPage(TextAnimation text)
        {
            mSource.screens[currentPage].text_animations.Add(text);
            SaveAndLoad(true);
        }

        void AddSpriteToCurrentPage(SpriteAnimation text)
        {
            mSource.screens[currentPage].sprite_animations.Add(text);
            SaveAndLoad(true);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        void FixIds()
        {
            for (int i = 0; i < mSource.screens.Count; i++)
            {
                int j = 0;
                foreach (var obj in mSource.screens[i].text_animations) obj.id = i + "_text_" + j++;
                j = 0;
                foreach (var obj in mSource.screens[i].sprite_animations) obj.id = i + "_sprite_" + j++;

            }

        }
        void RefreshTree()
        {
            treeView1.Nodes.Clear();
            TreeNode[] screens_array = new TreeNode[mSource.screens.Count];
            for (int i = 0; i < mSource.screens.Count; i++)
            {
                TreeNode node = new TreeNode(mSource.screens[i].name);
                screens_array[i] = node;
                node.Tag = mSource.screens[i].name;
                TreeNode[] texts_array = new TreeNode[mSource.screens[i].text_animations.Count];
                for (int j = 0; j < mSource.screens[i].text_animations.Count; j++)
                {
                    TreeNode txtNode = new TreeNode(mSource.screens[i].text_animations[j].id);
                    texts_array[j] = txtNode;
                    txtNode.Tag = mSource.screens[i].text_animations[j].id;
                }
                TreeNode text_anim_node = new TreeNode("text_animations");
                text_anim_node.Nodes.AddRange(texts_array);
                node.Nodes.Add(text_anim_node);
                TreeNode[] spr_array = new TreeNode[mSource.screens[i].sprite_animations.Count];
                for (int j = 0; j < mSource.screens[i].sprite_animations.Count; j++)
                {
                    TreeNode sprNode = new TreeNode(mSource.screens[i].sprite_animations[j].id);
                    spr_array[j] = sprNode;
                    sprNode.Tag = mSource.screens[i].sprite_animations[j].id;
                }
                TreeNode spr_anim_node = new TreeNode("sprite_animations");
                spr_anim_node.Nodes.AddRange(spr_array);
                node.Nodes.Add(spr_anim_node);
            }
            TreeNode treeNode = new TreeNode("screens", screens_array);
            treeNode.Tag = "0";
            treeView1.Nodes.Add(treeNode);

        }
        private void LoadProject()
        {
            if (currentProject == null) return;
            string site = "/" + currentProject + "/";
            using (StreamReader file = File.OpenText(mSettings.LocalHostPath + site + "/js/script.js"))
            {
                string text = file.ReadToEnd().Replace("var stage =", "");
                text = text.Replace(";", "");
                mSource = JsonConvert.DeserializeObject<Data>(text);
            }
            FixIds();

            if (mSource != null)
            {
                RefreshTree();
            }
            imageList1.Images.Clear();
            this.listView1.Items.Clear();
            DirectoryInfo directory = new DirectoryInfo(mSettings.LocalHostPath + site + "assets");
            FileInfo[] Archives = directory.GetFiles("*.png");
            foreach (FileInfo fileinfo in Archives)
            {
                imageList1.Images.Add(Image.FromFile(fileinfo.FullName));
            }
            this.listView1.View = View.LargeIcon;
            this.listView1.LargeImageList = this.imageList1;
            listView1.TileSize = new Size(128, 128);

            for (int j = 0; j < this.imageList1.Images.Count; j++)
            {
                ListViewItem item = new ListViewItem();
                item.ImageIndex = j;
                this.listView1.Items.Add(item);
            }

            this.AudioListView.Items.Clear();
            this.AudioListView.View = View.List;
            directory = new DirectoryInfo(mSettings.LocalHostPath + site + "audio/en");
            Archives = directory.GetFiles("*.mp3");
            foreach (FileInfo fileinfo in Archives)
            {
                ListViewItem item = new ListViewItem();
                item.Text = fileinfo.Name;
                this.AudioListView.Items.Add(item);
            }
            treeView1.ExpandAll();
            treeView1.SelectedNode = currentNode;
            ShowPage();
            AssetsData.FixAssetFile();
        }
        object FindObject(string tag, ref int page)
        {
            for (int i = 0; i < mSource.screens.Count; i++)
            {
                for (int j = 0; j < mSource.screens[i].text_animations.Count; j++)
                    if (mSource.screens[i].text_animations[j].id == tag) return mSource.screens[i].text_animations[j];
                for (int j = 0; j < mSource.screens[i].sprite_animations.Count; j++)
                    if (mSource.screens[i].sprite_animations[j].id == tag) return mSource.screens[i].sprite_animations[j];
                if (mSource.screens[i].name == tag) { page = i; return mSource.screens[i]; }
            }
            return mSource;
        }
        private void TreeView1_Click(object sender, System.Windows.Forms.TreeViewEventArgs e)
        {
            if (treeView1.SelectedNode.Tag != null)
            {
                int page = currentPage;
                object obj = FindObject(treeView1.SelectedNode.Tag.ToString(), ref page);
                currentPage = page;
                propertyGrid1.SelectedObject = obj;
                if (obj.GetType().Equals(typeof(Screen)))
                {
                    ShowPage();
                    webBrowser1.Update();
                }
            }

        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = mSettings.LocalHostPath.Replace("/", "\\");
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                currentProject = Path.GetFileName(Utility.FixURI(folderBrowserDialog1.SelectedPath));
                ReloadWebPage();
            }

        }


        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = mSettings.LocalHostPath.Replace("/", "\\");
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Utility.Copy(mSettings.LocalHostPath + "\\new", folderBrowserDialog1.SelectedPath);
                currentProject = Path.GetFileName(Utility.FixURI(folderBrowserDialog1.SelectedPath));
                ReloadWebPage();
            }
        }

        void ReloadWebPage()
        {
            webBrowser1.Navigate(new Uri(mSettings.LocalHost + currentProject + "?refreshToken=" + Guid.NewGuid().ToString()));
        }

        private void setProjectVarsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            propertyGrid1.SelectedObject = mSettings;
        }
        public void Test(String message)
        {
            MessageBox.Show(message, "client code");
        }
        private void ShowPage()
        {
            this.webBrowser1.Document.InvokeScript("CleanUp");
            this.webBrowser1.Document.InvokeScript("StartPage", new object[] { currentPage });
            this.webBrowser1.Document.InvokeScript("ShowScreen");
        }
        private void SaveAndLoad(bool reload)
        {
            string result = "var stage =";
            result += JsonConvert.SerializeObject(mSource, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            result += ";";
            string site = "/" + currentProject + "/";
            System.IO.File.WriteAllText(mSettings.LocalHostPath + site + "js/script.js", result);
            if(reload)
                ReloadWebPage();
        }

        private void viewSourceToolStripMenuItem_Click(object sender, EventArgs e)
        {

            using (ViewSourceDialog dialog = new ViewSourceDialog(System.IO.File.ReadAllText(mSettings.LocalHostPath + "/"+ currentProject + "/js/script.js")))
            {  dialog.ShowDialog();   }
        }
        public void UpdatePosition(string id, int x, int y)
        {
            for (int i = 0; i < mSource.screens.Count; i++)
            {
                foreach (var obj in mSource.screens[i].text_animations)
                {
                    if (obj.id == id)
                    {
                        obj.x = x;
                        obj.y = y;
                        SaveAndLoad(false);
                        return;
                    }
                }
                foreach (var obj in mSource.screens[i].sprite_animations)
                {
                    if (obj.id == id)
                    {
                        obj.x = x;
                        obj.y = y;
                        SaveAndLoad(false);
                        return;
                    }
                }

            }

        }
        object GetObjectFromJS(string id)
        {
            object sel_obj = null;
            foreach (var obj in mSource.screens[currentPage].text_animations)
            {
                if (obj.id == id) return obj;
            }
            foreach (var obj in mSource.screens[currentPage].sprite_animations)
            {
                if (obj.id == id) return obj;
            }
            return sel_obj;
        }
        public void SelectObject(string id)
        {
            object obj = GetObjectFromJS(id);
            if (obj != null)
                propertyGrid1.SelectedObject = obj;
        }

        private void RemovePageMenu_Click(object sender, EventArgs e)
        {
            mSource.screens.Remove(mSource.screens[currentPage]);
            RefreshTree();
            SaveAndLoad(true);
        }

        private void AddPageMenu_Click(object sender, EventArgs e)
        {
            Screen scr = new Screen();
            mSource.screens.Add(scr);
            RefreshTree();

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAndLoad(true);
        }

        private void AudioListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            waveOut.Stop();
            if (AudioListView.SelectedItems.Count <= 0)
                return;
            Mp3FileReader reader = new Mp3FileReader(Utility.CurrentProjectPath + "/audio/en/" + AudioListView.SelectedItems[0].Text);
            waveOut.Init(reader);
            waveOut.Play();
        }
    }
}
