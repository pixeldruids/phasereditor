﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KfdGen
{
    public partial class AddTextDialog : Form
    {
        public TextAnimation textAnim = new TextAnimation();
        public AddTextDialog(TextAnimation anim)
        {
            textAnim = anim;
            Init();
        }
        public AddTextDialog()
        {
            Init();
        }
        void Init()
        {
            InitializeComponent();
            richTextBox1.Text = "Text . . .";
            textBox1.Text = "200";
            textBox2.Text = "100";
            textBox3.Text = "200";
            textBox4.Text = "200";
            textBox5.Text = "15";
            textBox6.Text = "1";
            textBox7.Text = "0";
            comboBox1.SelectedIndex = 0;

            textBox1.KeyPress += NumericTB_Keypress;
            textBox2.KeyPress += NumericTB_Keypress;
            textBox3.KeyPress += NumericTB_Keypress;
            textBox4.KeyPress += NumericTB_Keypress;
            textBox5.KeyPress += NumericTB_Keypress;
            textBox6.KeyPress += NumericTB_Keypress;
            textBox7.KeyPress += NumericTB_Keypress;
        }
        private void NumericTB_Keypress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void txtDialogOK_Click(object sender, EventArgs e)
        {
            textAnim.text.Add(richTextBox1.Text);
            textAnim.sx = int.Parse(textBox1.Text);
            textAnim.sy = int.Parse(textBox2.Text);
            textAnim.x = int.Parse(textBox3.Text);
            textAnim.y = int.Parse(textBox4.Text);
            textAnim.size = int.Parse(textBox5.Text);
            textAnim.timing = int.Parse(textBox6.Text);
            textAnim.delay = int.Parse(textBox7.Text);
            string tween_type = comboBox1.Text;
            if (string.IsNullOrEmpty(tween_type))
                tween_type = "Elastic";          
            textAnim.tween_type = tween_type;
        }

    }
}
