﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KfdGen
{
    abstract class Asset
    {
        public string name;
        public string path;
        public string prefixStr ;
        public string postfixStr = ");";

        public abstract string GetString();
    }
    class AudioAsset : Asset
    {
        public AudioAsset()
        {
            prefixStr = "game.load.audio(";
        }
        public void Init( string data)
        {

        }
        public override string GetString()
        {
            return prefixStr + "'" + name + "' , ['audio/'" + lang + "'/'" + path + "''mp3'" + postfixStr;
        }
        string lang = "en";
    }
    class SpriteAsset : Asset
    {
        public SpriteAsset()
        {
            prefixStr = "game.load.spritesheet(";
        }
        public void Init(string data)
        {
            string temp = "";
            temp = data.Replace(prefixStr, "");
            temp = temp.Replace(postfixStr, "");
            string[] param_list = temp.Split(',');
            name = param_list[0];
            path = param_list[1];
            width = int.Parse(param_list[2]);
            height = int.Parse(param_list[3]);
        }
        public override string GetString()
        {
            name = name.Replace("'", "").Trim();
            path = path.Replace("'", "").Trim();
            return prefixStr + "'" + name + "', '" + path + "', " + width + ", " + height + postfixStr;
        }
        public int width;
        public int height;
    }
    public class Assets
    {
        List<Asset> AssetList = new List<Asset>();
        string AssetFile = "";
        string prefix = "function PreloadAssets()\n{";
        string postfix = "}";

        public void Init(string file_name)
        {
            AssetFile = file_name;
            AssetList.Clear();
            string []content = File.ReadAllLines(file_name);
            SpriteAsset tempSprite = new SpriteAsset();
            AudioAsset tempAudio = new AudioAsset();

            foreach(string line in content)
            {
                if(line.Trim().StartsWith(tempSprite.prefixStr))
                {
                    SpriteAsset sprite = new SpriteAsset();
                    sprite.Init(line.Trim());
                    AssetList.Add(sprite);
                }
            }
        }

        public void Generate()
        {
            using (StreamWriter writer =
                       new StreamWriter(AssetFile))
            {
                writer.WriteLine(prefix);
                foreach(Asset asset in AssetList)
                    writer.WriteLine(asset.GetString());
                writer.WriteLine(postfix);
            }
        }

        public void AddSprite(string path, int width, int height)
        {
            string name = Path.GetFileNameWithoutExtension(path);
            path = Utility.FixURI(path);
            SpriteAsset sprite = new SpriteAsset();
            sprite.name = name;
            sprite.path = Utility.GetRelativePath(path, "assets");            
            sprite.width = width;
            sprite.height = height;
            AssetList.Add(sprite);
            Generate();
        }
        public void FixAssetFile()
        {
            foreach(Asset asset in AssetList)
            {
                SpriteAsset sprite = (SpriteAsset)asset;
                if (sprite != null)
                {
                    sprite.name = Path.GetFileNameWithoutExtension(sprite.path);
                }
            }
            Generate();
            Init(Utility.CurrentAssetsFile);
        }
    }
}
