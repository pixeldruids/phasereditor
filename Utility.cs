﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KfdGen
{
    class Utility
    {
     //   public static string CurrentProject;

        public static string CurrentProject = "";
        public static string CurrentProjectPath = "";
        public static string CurrentJsFolder = "";
        public static string CurrentAssetsFile = "";

        public static Image cropImage(Image img, Rectangle cropArea)
        {
            Bitmap bmpImage = new Bitmap(img);
            Bitmap bmpCrop = bmpImage.Clone(cropArea, bmpImage.PixelFormat);
            return (Image)(bmpCrop);
        }

        public static void Copy(string sourceDirName, string destDirName, bool skip_if_present = true, bool copySubDirs = true)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the source directory does not exist, throw an exception.
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory does not exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }


            // Get the file contents of the directory to copy.
            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                // Create the path to the new copy of the file.
                string temppath = Path.Combine(destDirName, file.Name);

                // Copy the file.
                if (skip_if_present && !File.Exists(temppath))
                    file.CopyTo(temppath, false);
            }

            // If copySubDirs is true, copy the subdirectories.
            if (copySubDirs)
            {

                foreach (DirectoryInfo subdir in dirs)
                {
                    // Create the subdirectory.
                    string temppath = Path.Combine(destDirName, subdir.Name);

                    // Copy the subdirectories.
                    Copy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
        public static string FixURI(string uri)
        {
            uri = uri.Replace("\\\\", "/");
            uri = uri.Replace("\\", "/");
            return uri;
        }

        public static string GetRelativePath(string uri, string path_type)
        {
            if (path_type == "assets")
            {
                if (uri.Contains(Utility.CurrentProject))
                {
                    int index = uri.IndexOf("/" + Utility.CurrentProject + "/");
                    return uri.Substring(index + 2 + Utility.CurrentProject.Length);
                }
                else
                {
                    Console.Write(" Path Doesn't contain current project. so copying resource to relevent path");                    
                    return CopyAssetToProject(uri, path_type); 
                }
            }
            return uri;
        }

        static string CopyAssetToProject(string uri, string path_type)
        {
            string dest = Utility.CurrentProjectPath + "/" + path_type + "/" + Path.GetFileName(uri);
            File.Copy(uri, dest, true);
            return  path_type + "/" + Path.GetFileName(uri);
        }

    }
}
