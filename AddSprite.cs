﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KfdGen
{
    public partial class AddSprite : Form
    {
        public SpriteAnimation result = new SpriteAnimation();
        GlobalSettings mSettings = new GlobalSettings();
        Image OriginalImage;
        int width = 1;
        int height = 1;
        int currentFrame = 0;
        List<Image> FrameList = new List<Image>();
        int x = 0, y = 0;
        string imageLocation = "";
        public AddSprite(int x, int y)
        {
            InitializeComponent();
            this.x = x;
            this.y = y;
            for(int i = 1; i < 16; i++)
            {
                comboBox1.Items.Add(i);
                comboBox2.Items.Add(i);
            }
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;
            mSettings.Init();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openSpriteDialog.InitialDirectory = mSettings.LocalHostPath;
            if (openSpriteDialog.ShowDialog() == DialogResult.OK)
            {
                imageLocation = openSpriteDialog.FileName;
                OriginalImage = Image.FromFile(imageLocation);
                pictureBox1.BackgroundImage = OriginalImage;
                pictureBox2.BackgroundImage = OriginalImage;
                FrameList.Clear();
                FrameList.Add(OriginalImage);
                comboBox1.SelectedIndex = 0;
                comboBox2.SelectedIndex = 0;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (OriginalImage == null) return;
            if (FrameList.Count < 1) return;
            currentFrame = ++currentFrame % FrameList.Count;
            pictureBox1.BackgroundImage = FrameList[currentFrame];
        }
        
        void SetupSprite(int row_val, int col_val)
        {
            if (OriginalImage == null)
                return;
            int row = row_val;
            int col = col_val;
            Image img = OriginalImage;
            width = img.Width / col;
            height = img.Height / row;
            if (width < 1) width = 1;
            if (height < 1) height = 1;
            FrameList.Clear();
            int no_of_frames = row * col;
            for (int i = 0; i < no_of_frames; i++)
            {
                int c = i / row;
                int r = i % row;
                if (r * width < (img.Width - width) && c * height < (img.Height - height))
                    FrameList.Add(Utility.cropImage(OriginalImage, new Rectangle(r * width, c * height, width, height)));
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(comboBox1.Text) || String.IsNullOrEmpty(comboBox2.Text))
                return;
            SetupSprite(int.Parse(comboBox1.Text), int.Parse(comboBox2.Text));
        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            result.x = x;
            result.y = y;
            result.timing = float.Parse(textBox6.Text);
            result.delay = float.Parse(textBox7.Text);
            result.sprite = Path.GetFileNameWithoutExtension(imageLocation);
            result.loop = LoopCB.Checked;
            result.scale = float.Parse(textBox5.Text);
            Form1.AssetsData.AddSprite(imageLocation, OriginalImage.Width / int.Parse(comboBox2.Text), OriginalImage.Height / int.Parse(comboBox1.Text));
        }
    }
}
