﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.Design;

namespace KfdGen
{
    class GlobalSettings
    {
        string _folderName = null;
        string _httpPath = null;
        [Editor(typeof(System.Windows.Forms.Design.FolderNameEditor),
                typeof(System.Drawing.Design.UITypeEditor))]
        public string LocalHostPath
        {
            get { return _folderName; }
            set { _folderName = value; Properties.Settings.Default.LocalHostPath = _folderName; Properties.Settings.Default.Save(); }
        }

        public string LocalHost
        {
            get { return _httpPath; }
            set { _httpPath = value; Properties.Settings.Default.LocalHost = _httpPath; Properties.Settings.Default.Save(); }
        }
        public void Init()
        {
            LocalHostPath = Utility.FixURI(Properties.Settings.Default.LocalHostPath);
            LocalHost = Utility.FixURI(Properties.Settings.Default.LocalHost);
        }
    }
}
