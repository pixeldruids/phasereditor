var game = new Phaser.Game(800, 480, Phaser.AUTO, 'Bharti AXA', { preload: preload, create: create});
var StartX  = -1000, count = 0,SfxIndex = 0,offset = 0, sfx_offset = 0,cur_scr = 0,currentTween,currentSound,currentTimer;
var obj_list = new Array();
var group,years;
var cur_sync_point = "";
var screen_list = [];
var cur_sfx_list  = new Array();
var events_list = [];
var lang = 'en';
function preload() {
	PreloadAssets();
	PreloadAudio();
}
function create() {
	game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
	game.stage.backgroundColor = "#CDEDFA";
	group = game.add.group();
	group.inputEnableChildren = true;
	game.onPause.add(onGamePause, this);
    game.onResume.add(onGameResume, this);
	game.world.bringToTop(group);
	game.stage.smoothed = false;
	game.stage.disableVisibilityChange = true;
	this.game.scale.pageAlignHorizontally = true;
	this.game.scale.pageAlignVertically = true;
	this.game.scale.refresh();
	actionOnClick();
}
function SetupText(obj, x,y, txt, font_size, alig, weight)
{
	font_size = font_size || 27;
	alig = alig || "center";
	weight = weight || "bold"; 
	obj  = game.add.text(x, y, txt, {
        font: font_size + "px Arial",
        fill: "#2A59A6",
        align: alig,
		fontWeight: weight,
    });
	group.add(obj);
	obj.anchor.setTo(0.5, 0.5);
	obj.inputEnabled = true;
	obj.input.enableDrag();
	obj.events.onDragStop.add(onDragStop, this);
	obj.events.onInputDown.add(onInputDown, this);
	return obj;
}
function typeWriter(obj, txt, interval, n,  cb){  
	n = n || 0;
	cb = cb || null;
	if(n == 0) obj.text = "";
	if(n<txt.length)
	{  
		obj.text += txt.charAt(n);
		n++;
		setTimeout(function(){typeWriter(obj, txt, interval, n, cb);game.world.bringToTop(group);},interval);
	}
	else
		if (cb != null)	cb.call(this);
	return obj;
}

function LoadAnimation(obj, x, y, sprite_name, anim_complete_cb, anim_name)
{
	anim_complete_cb = anim_complete_cb || null;
	anim_name = anim_name || "default";
	if (!obj)
	{
		obj = game.add.sprite( 0, 0, sprite_name);
		obj.anchor.set(0.5);
		var anim = obj.animations.add(anim_name);
		if (anim_complete_cb != null)
			anim.onComplete.add(anim_complete_cb, this);
	}
	obj.x = x;
	obj.y = y;
	obj.inputEnabled = true;
	obj.input.enableDrag();
	obj.events.onDragStop.add(onDragStop, this);
	obj.events.onInputDown.add(onInputDown, this);
	return obj;
}
function onGamePause()
{
	if(currentTween != null)currentTween.pause();
	if(currentSound != null)currentSound.pause();
	if(currentTimer != null)currentTimer.pause();
}
function onGameResume()
{
	if(currentTween != null)currentTween.resume();
	if(currentSound != null)currentSound.resume();
	if(currentTimer != null)currentTimer.resume();
}
function AddEvent(ev)
{
	events_list[events_list.length] = ev;
}
function PlaySpriteAnim(anim)
{
	AddEvent(game.time.events.add(Phaser.Timer.SECOND * anim.delay, function(){
		var temp = LoadAnimation(temp, anim.x, anim.y, anim.sprite);
		temp.id = anim.id;
		temp.fn_type = anim.fn_type;
		temp.fn_param = anim.fn_param;
		if(anim.fn_type)
			temp.input.useHandCursor = true;
		temp.play('default', anim.timing, anim.loop);
		if(anim.scale)
			temp.scale.set(anim.scale);
		obj_list.push(temp);
		schedule_tween(temp);
	}, this));	
	
}
function PlayTextAnim(anim)
{
	var text_toDisplay = "";
	for(var i = 0; i < anim.text.length; i++ ) 
	{
		if (anim.text[i].startsWith("window.")) 
		{
			var str = eval(anim.text[i]);
			if(anim.ConvertToIndian){ str = ConvertToIndian(str);}
			if (anim.add_audio_delay) { var str_in_words = convertToWords(str); sfx_offset += str_in_words.length * 0.5; }
			text_toDisplay += str;
		}
		else text_toDisplay += anim.text[i];
	}
	var tween_type = anim.tween_type.toLowerCase();
	if(tween_type.startsWith("typewrite")) 
	{
		anim.sx = anim.x; anim.sy = anim.y;
	}
	var txt = SetupText(txt,anim.sx, anim.sy, text_toDisplay, anim.size);
	txt.id = anim.id;	
	if(anim.anchor != null){	txt.anchor.setTo(anim.anchor, 0.5)}
	
	if(tween_type.startsWith("typewrite"))
	{
		txt.text = "";
		 AddEvent(game.time.events.add(Phaser.Timer.SECOND * anim.delay, function(){txt = typeWriter(txt, text_toDisplay, 50);}, this));
	}
	 else
		game.add.tween(txt).to({x:anim.x, y: anim.y}, anim.timing, anim.tween_type, true, Phaser.Timer.SECOND * anim.delay);	
	txt.fn_type = anim.fn_type;
	txt.fn_param = anim.fn_param;
	if(anim.fn_type)
		txt.input.useHandCursor = true;

	obj_list.push(txt);
}
function schedule_tween(obj)
{
	if(!stage.screens[cur_scr].tweens) return;
	 for(var i = 0; i < stage.screens[cur_scr].tweens.length; i++)
	 {
		 var tween = stage.screens[cur_scr].tweens[i];
		 if(tween.obj.startsWith(obj.key))
		 {
			 game.add.tween(obj).to( {x: tween.x, y: tween.y }, tween.timing, tween.tween_type, true, Phaser.Timer.SECOND * tween.delay);
			 break;
		 }
	 }
}
function OnStopCB(v)
{
	return function() {	if (cur_sfx_list[v + 1]) currentSound = cur_sfx_list[v + 1].play()};
}
function PlaySound(anim)
{
	cur_sfx_list.length = 0;
	for(var i = 0; i < anim.length; i++)
		cur_sfx_list.push(game.add.audio(anim[i].sound));
	var v = 0;
	for(var i = 0; i < cur_sfx_list.length - 1; i++)
	{
		v = i;
		cur_sfx_list[i].onStop.add(OnStopCB(i));	
	}
	if (cur_sfx_list.length > 0) currentSound = cur_sfx_list[0].play();

	
}
function actionOnClick()
{
	//window.external.Test('called from script code');
	StartPage(0);
	ShowScreen();
}
function testCall()
{
	//alert("testcall");
}
function onDragStop(sprite, pointer) {
	if(sprite.id)
	window.external.UpdatePosition(sprite.id, pointer.x, pointer.y);
}
function onInputDown(sprite, pointer) {
	if(sprite.id)
		window.external.SelectObject(sprite.id);
	if("navigate" == sprite.fn_type)
	{
		CleanUp();
		StartPage(sprite.fn_param);
		ShowScreen();
	}
}
function ShowScreen()
{
    sfx_offset = 0;
	game.sound.stopAll();
	if(stage.screens.count <= 0)
		return;
	if (stage.screens[cur_scr].sprite_animations != null)
	 for(var i = 0; i < stage.screens[cur_scr].sprite_animations.length; i++)
		 PlaySpriteAnim(stage.screens[cur_scr].sprite_animations[i]);
	if (stage.screens[cur_scr].text_animations  != null)
	 for(var i = 0; i < stage.screens[cur_scr].text_animations.length; i++)
		 PlayTextAnim(stage.screens[cur_scr].text_animations[i]);
	if (stage.screens[cur_scr].sound_list != null)	 
	 PlaySound(stage.screens[cur_scr].sound_list);
/*	 for(var i = 0; i < stage.screens[cur_scr].sound_list.length; i++)
		 PlaySound(stage.screens[cur_scr].sound_list[i]);*/
	 if(stage.screens[cur_scr].functions  != null)
	 for(var i = 0; i < stage.screens[cur_scr].functions.length; i++)
		 call_fn(stage.screens[cur_scr].functions[i]);
	 if (stage.screens[cur_scr].timing >= 0)
		game.time.events.add(Phaser.Timer.SECOND * (stage.screens[cur_scr].timing + sfx_offset), TransitScreen, this);
}
function CleanUp()
{
	for(var i = 0; i < obj_list.length; i++)
	{
		obj_list[i].x = -game.width * 2;	
		obj_list[i].destroy();
	}
	for(var i = 0; i < events_list.length; i++)
	{
		game.time.events.remove(events_list[i]);
	}
}
function TransitScreen()
{
	CleanUp();
	cur_scr++;
	if (cur_scr < stage.screens.length)ShowScreen();
}
function Init(){	}
function call_fn(func){	AddEvent(game.time.events.add(Phaser.Timer.SECOND * func.delay, function(){eval(func.fn)}, this));}
function gofull() { game.scale.isFullScreen ? game.scale.stopFullScreen():game.scale.startFullScreen(false);}
function SetBGColor(color){	game.stage.backgroundColor = color;}
function StartPage(num) { cur_scr = num;}
String.prototype.startsWith = function (str){ return this.indexOf(str) == 0;}
function find(str, sub){if (str.indexOf(sub) == -1)return false; 	return true;}
